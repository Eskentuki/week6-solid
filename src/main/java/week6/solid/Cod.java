package week6.solid;

public class Cod extends Bone {

    public Cod(String name, String color) {
        super(name, color);
    }

    @Override
    public void swim() {
        swim();
    }

    @Override
    public void bubble() {
        bubble();
    }

    @Override
    public void say() {
        System.out.println("I am a cod fish, bone fish. My name is " + getName() +", my color is " + getColor());
    }
}

