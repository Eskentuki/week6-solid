package week6.solid;

public interface Fish extends Creature{
    void swim();
    void bubble();
}
