package week6.solid;

public class Herring extends Bone {

    public Herring(String name, String color) {
        super(name, color);
    }

    @Override
    public void swim() {
        swim();
    }

    @Override
    public void bubble() {
        bubble();
    }

    @Override
    public void say() {
        System.out.println("I am a herring, bone fish. My name is " + getName() +", my color is " + getColor());
    }
}