package week6.solid;

public class Shark extends Cartilaginous {

    public Shark(String name, String color) {
        super(name, color);
    }

    @Override
    public void swim() {
        swim();
    }

    @Override
    public void bubble() {
        bubble();
    }

    @Override
    public void say() {
        System.out.println("I am a shark, cartilaginous fish. My name is " + getName() +", my color is " + getColor());
    }
}