package week6.solid;

public interface Reptile extends Creature{
    void crawl();
    void roar();
}
