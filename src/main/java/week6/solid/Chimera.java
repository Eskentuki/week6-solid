package week6.solid;

public class Chimera extends Cartilaginous {

    public Chimera(String name, String color) {
        super(name, color);
    }

    @Override
    public void swim() {
        swim();
    }

    @Override
    public void bubble() {
        bubble();
    }

    @Override
    public void say() {
        System.out.println("I am a chimera, cartilaginous fish. My name is " + getName() +", my color is " + getColor());
    }
}