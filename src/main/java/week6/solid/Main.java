package week6.solid;

public class Main {

    public static void main(String[] args) {
        Aquarium aquarium = new Aquarium();
        aquarium.addCreature(new Chimera("Lenny","red"));
        aquarium.addCreature(new Cod("Oscar","blue"));
        aquarium.addCreature(new Herring("Dante ","white"));
        aquarium.addCreature(new Lizard("Riaz ","green"));
        aquarium.addCreature(new Snake("Hughes","black"));
        aquarium.addCreature(new Stingray("Lola ","pink"));
        aquarium.addCreature(new Shark("Colleen ","gray"));

        aquarium.start();
    }
}