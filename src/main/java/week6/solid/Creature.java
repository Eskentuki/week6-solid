package week6.solid;

public interface Creature{
    String getName();
    String getColor();
    void setName(String name);
    void setColor(String color);
    void say();
}
