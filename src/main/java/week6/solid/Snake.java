package week6.solid;

public class Snake extends Scaly {

    public Snake(String name, String color) {
        super(name, color);
    }

    @Override
    public void crawl() {
        crawl();
    }

    @Override
    public void roar() {
        roar();
    }

    @Override
    public void say() {
        System.out.println("I am snake from scaly reptiles. My name is " + getName() +", my color is " + getColor());
    }
}