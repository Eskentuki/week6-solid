package week6.solid;

public class Stingray extends Cartilaginous {

    public Stingray(String name, String color) {
        super(name, color);
    }

    @Override
    public void swim() {
        swim();
    }

    @Override
    public void bubble() {
        bubble();
    }

    @Override
    public void say() {
        System.out.println("I am a stingray, cartilaginous fish. My name is " + getName() +", my color is " + getColor());
    }
}