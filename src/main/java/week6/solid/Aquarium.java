package week6.solid;

import java.util.LinkedList;
import java.util.List;

public class Aquarium {
    private List<Creature> creatures = new LinkedList<>();

    public void addCreature(Creature creature) {
        creatures.add(creature);
    }

    public void start(){
        creatures.forEach(c -> c.say());
    }
}