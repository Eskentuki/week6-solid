package week6.solid;

public abstract class Cartilaginous implements Fish{
    private String name;
    private String color;

    public Cartilaginous(String name, String color) {
        setName(name);
        setColor(color);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getColor(){
        return color;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }
}
