package week6.solid;

public class Lizard extends Scaly {
    public Lizard(String name, String color) {
        super(name, color);
    }

    @Override
    public void crawl() {
        crawl();
    }

    @Override
    public void roar() {
        roar();
    }

    @Override
    public void say() {
        System.out.println("I am lizard from scaly reptiles. My name is " + getName() +", my color is " + getColor());
    }
}
